/*图书简介-收藏*/
function collectBook(a) {
	if($(a).hasClass("active") == true) {
		$(a).removeClass("active");
		$(a).find(".icon-heart").css("color", "#989898");
		$(a).find("p").html("收藏");
	} else {
		$(a).addClass("active")
		$(a).find(".icon-heart").css("color", "#fd6951");
		$(a).find("p").html("已收藏");

		$(".collectLi").click(function() {
			$(".pop_cancelCollect").show();
			$("#popBg").show();
		})

	}

}

/*关闭弹窗*/
function popDown() {
	$(".popUp").hide();
}

/*打开弹窗*/
/*测评页面-打开弹窗*/
function popUpCeping1() {
	$(".popUp.ceping_popUp").show();
	$("#popBg1").show();
}

/*12-0读后感详情-有编辑按钮.html-打开弹窗*/
function popUpReadReport1() {
	$(".popUp.pop_cancelCollect").show();
	$("#popBg").show();
}
/*4-0图书详情.html--展开阅读*/
$(".bookDetail_intro_zhankai").click(function(){
	$(".bookDetail_introP").toggleClass("bookDetail_introP1");
	$(this).toggleClass("rorateIcon");
})
/*5-0老师-测评.html*/
/*测评页面-打开弹窗*/
function popUpReadReport2() {
	$(".popUp.ceping_popUp").hide();
	$(".ceping_popUp #popBg1").hide();
	$(".popUp.pop_cancelCollect").show();
	$(".pop_cancelCollect #popBg1").show();
}

/*8-老师-任务.html-打开删除弹窗*/
function popUpReadReport3() {
	$(".popUp.ceping_popUp").hide();
	$("#popBg1").hide();
	
	$(".popUp.pop_cancelCollect").show();
	$("#popBg").show();
	
}