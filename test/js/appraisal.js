// 单选题
function singleChoice() {
    $('.topic dd a').click(function() {
        $('.topic dd a').attr("class", "");
        $(this).toggleClass('current');
    })
}
// 多选题
function multipleChoice() {
    $('.topic dd a').click(function() {
        $(this).toggleClass('current');
    })
}
// 图片选择
function imgChoice() {
    $('.img_list .row .list').click(function() {
        $(this).toggleClass('current');
    });
    var imgW = $('.list img').width();
    $('.list img').css('height', imgW * 1.4 + 'px');
}
